import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title:"PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home:Scaffold(
        appBar:AppBar (
          leading: Icon(Icons.account_balance),
          title :Text("6350110018"),
          actions: [
            IconButton(
                onPressed:(){},
                icon: Icon(Icons.add_a_photo),
            ),
            IconButton(
              onPressed:(){},
              icon: Icon(Icons.accessibility),
            ),
          ],
        ),
        body:Center(
          child:Column(
            children:[
              // Image.asset("assets/โอ๊ต.jpg",
              //   height: 500,
              //   width: 300,
              // ),

              CircleAvatar(
                backgroundImage: AssetImage("assets/โอ๊ต.jpg",),
                radius: 150,
              ),

              Text (
                  "Suphakorn Sinsupparoek",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),

        ),
      ),
    );
  }
}